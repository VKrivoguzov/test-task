#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <thread>
#include <functional>
#include <Process.hpp>

void childProcessRoutine(uint16_t port, int32_t count){
        struct sockaddr_in addr;
        int sock = socket(AF_INET, SOCK_STREAM,0);
        if(sock<0)
            exit(1);        

        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = inet_addr("127.0.0.1");
        if(connect(sock, (struct sockaddr*)&addr, sizeof(addr)) <0){
            std::cout << "connection failure";
            std::cout << errno;
            exit(1);
        }
        while(true){     
           send(sock, &count, sizeof(count),0);
           count++;
        }
}

void changeProcessNumber(std::vector<Process>& v, int n, int port){
        int diff = n - v.size();
        if(diff<0)
        {
            for(;diff!=0;diff++){
                v.back().terminate();
                v.back().wait();                   
                v.pop_back();
            }
        }
        else
        {
            for(;diff!=0;diff--)
            {
                v.push_back(Process(std::bind(childProcessRoutine, port, 0))); 
            }
        }
}

void input(std::vector<Process>& v, int& n, int port){
    while(n!=0){
        std::cout<< "Enter number of processes (0 for exit):\n";
        changeProcessNumber(v, n, port);
        std::cin >> n;        
    }    
}

int main(){
    using namespace std;
    const int max_clients = 100;
    const int port = 5555;

    cout<< "Enter number of processes (0 for exit):\n";
    int n = 0;
    cin >> n; 

    struct sockaddr_in addr;
    int listener = socket(AF_INET, SOCK_STREAM,0);
    if(listener < 0){
        cout<<"socket error";
        exit(1);
    }

    fcntl(listener, F_SETFL, O_NONBLOCK);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if(bind(listener, (struct sockaddr*)&addr, sizeof(addr)) <0){
        cout<< "bind error ";
        cout<< errno << endl;
        exit(1);
    }

    if(listen(listener,max_clients)<0)
    {
        cout<< "bind error ";
        cout<< errno << endl;
        exit(1);
    }
    
    vector<Process> processes;
    changeProcessNumber(processes, n, port);
    thread t(input,ref(processes),ref(n),port);
    
    map<int,uint32_t> fdToCounter;
    ofstream out("output");
    fd_set readset;
    int32_t buf[256];
    while(n!=0){
        FD_ZERO(&readset);
        FD_SET(listener, &readset);
        
        int max = listener;
        for(auto p: fdToCounter){
            max = std::max(max,p.first);
            FD_SET(p.first, &readset);
        }
        
        int activity = select(max+1, &readset, nullptr, nullptr, nullptr);
        if(activity < 0){
            cout<< "select error ";
            cout<< errno << endl;
            exit(1);
        }
        if(activity == 0)
            continue;

        if(FD_ISSET(listener, &readset)){
            int sock = accept(listener, nullptr, nullptr);
            if(sock<0)
            {
                cout<<"accept error";
            }
            fcntl(sock, F_SETFL, O_NONBLOCK);
            fdToCounter[sock] = 0;
        }    

        for(auto it = fdToCounter.begin(); it!=fdToCounter.end();)
        {
            
            if(FD_ISSET(it->first, &readset))
            {   
                size_t bytes_read = recv(it->first, &buf, sizeof(buf),0);

                if(bytes_read <= 0){
                    close(it->first);
                    fdToCounter.erase(it++);
                    continue;   
                }
                it->second = buf[bytes_read/sizeof(int32_t)-1]; //get last received number              
            }
            ++it;
        }
        out.seekp(ios::beg);
        int processNumber = 0;
        for(auto& p: fdToCounter){            
            out << "Process "
                << setw(3) << ++processNumber << ": " 
                << setw(10) << p.second << endl;
        }        
    }
   
    changeProcessNumber(processes,0,0);      
    t.join();
    return 0;
}