#include "Process.hpp"

Process::Process(std::function<void()> processRoutine,bool deferStart): processRoutine_(processRoutine)
{
    if(!deferStart)
        start();
}

Process::Process(Process&& rhs) noexcept
: processRoutine_(std::move(rhs.processRoutine_)),
    pid_(rhs.pid_),
    isStarted_(rhs.isStarted_)
{
    rhs.isStarted_ = false;
}

Process& Process::operator=(Process&& rhs) noexcept
{
    processRoutine_ = std::move(rhs.processRoutine_);
    pid_ = rhs.pid_,
    isStarted_ = rhs.isStarted_;
    rhs.isStarted_ = false;       
    return *this;
}

pid_t Process::pid() const
{
    return pid_;
}
bool Process::start() noexcept
{
    if(isStarted_) return false;

    pid_ = fork();
    if(pid_ == -1) return false;       
    if(pid_ == 0){
        isStarted_ = true;
        processRoutine_();
        exit(0);
    } 
    return true;    
}
bool Process::isRunning() const noexcept
{
    if(waitpid(pid_, nullptr, WNOHANG) == 0)
        return true;
    return false;
}

int Process::terminate() noexcept
{
    if(kill(pid_,SIGKILL) == -1)
            return errno;
    return 0;
}

int Process::wait() noexcept
{
    if(waitpid(pid_, nullptr, 0) == -1)
            return errno;
    return 0;
}

Process::~Process()
{        
    if(isStarted_ && isRunning())
        terminate();
}