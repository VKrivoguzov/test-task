CXX		  := g++
CXX_FLAGS := -Wall -Wextra -std=c++17 -ggdb -pthread

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:=
EXECUTABLE	:= main


all: $(LIB) $(BIN) $(BIN)/$(EXECUTABLE) 

run: clean all
	clear 
	./$(BIN)/$(EXECUTABLE)

$(LIB):
	mkdir ./$(LIB)

$(BIN):
	mkdir ./$(BIN)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp 
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*
