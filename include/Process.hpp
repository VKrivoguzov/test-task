#pragma once
#include <functional>
#include <unistd.h>
#include <system_error>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>

class Process {

public:
    Process(std::function<void()> processRoutine,bool deferStart = false);

    Process(const Process&) = delete;
    Process& operator=(const Process&) = delete;
    
    Process(Process&& rhs) noexcept;
    Process& operator=(Process&& rhs) noexcept;
    
    //
    pid_t pid() const;
    bool start() noexcept;
    bool isRunning() const noexcept;
    int terminate() noexcept;
    int wait() noexcept;
    
    ~Process();
private:
    std::function<void()> processRoutine_;
    pid_t pid_;
    bool isStarted_ = false;
};